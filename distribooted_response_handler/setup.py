from distutils.core import setup
setup(
  name = 'distribooted_response_handler',
  packages = ['distribooted_response_handler'], # this must be the same as the name above
  version = '0.1',
  description = 'response helper functions for distribooted',
  author = 'Borislav Sevcik',
  author_email = 'boriss@ballab.com',
  url = 'https://github.com/peterldowns/mypackage', # use the URL to the github repo
  download_url = 'https://github.com/sliche/distribooted_response_handler/archive/0.1.tar.gz', # I'll explain this in a second
  keywords = ['distribooted', 'response', 'handler', 'flask'], # arbitrary keywords
  classifiers = [],
)
